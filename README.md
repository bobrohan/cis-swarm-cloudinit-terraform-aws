# CIS (Center for Internet Security) Swarm 
## cloudinit, terraform, aws

Single node master+worker swarm for implementation of https://www.cisecurity.org/benchmark/docker/

## Prereqs 

Add the following to `terraform.tfvars` 

```
accesskey=
secretkey=
dreamteam-pubkey=
dreamteam-dburi=
```

## One command deploy
 
`terraform apply --auto-approve`

## Running Docker Bench for Security

As per https://github.com/docker/docker-bench-security

```
   docker run -it --net host --pid host --userns host --cap-add audit_control \
    -e DOCKER_CONTENT_TRUST=$DOCKER_CONTENT_TRUST \
    -v /etc:/etc:ro \
    -v /usr/bin/docker-containerd:/usr/bin/docker-containerd:ro \
    -v /usr/bin/docker-runc:/usr/bin/docker-runc:ro \
    -v /usr/lib/systemd:/usr/lib/systemd:ro \
    -v /var/lib:/var/lib:ro \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    --label docker_bench_security \
    docker/docker-bench-security
```